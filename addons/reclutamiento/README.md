## Reclutamiento y seleccion

### Requisitos funcionales
- Debe almacenar la necesidad de personal y el tipo de contratación (obrero, concurso, encuesta, proyecto, alto nivel). (Si)
- Debe guardar los currículum y poder acceder a ellos. (Si)
- Debe permitir el establecimiento de la fecha de entrevista.
- Según el tipo de contratación, recibirá diferentes documentos.
- Debe almacenar y permitir visualizar los documentos del postulado. (almacena pero no visualiza)
- Debe permitir elaborar la solicitud de ingresos.
- Debe permitir notificar las inconsistencias.

### Requisitos no funcionales
- Solo puede ser usado por el personal designado.
- Debe tener un desempeño óptimo.
- Necesita Interoperabilidad.
- Los datos se deben almacenar en la base de datos.
- La licencia del código sera de tipo software libre.

### Clases:
- Postulado
    - nombre
    - apellido
    - tipo_empleo_id (M2o)
    - correo
    - curriculum
    - aprobacion (Boolean)
    - telefonos
    - entrevistas_id (M2o)
    - cop_cedula (Boolean)
    - cop_titulo (Boolean)
    - cop_trabajo (Boolean)
    - cop_militar (Boolean)
    - copia_titulo (Binary)
    - constancia_trabajo (Binary)
    - constancia_militar (Binary)
    - copia_cedula (Binary)
    - solicitud_postulacion (Binary)
    - active (boolean)

- Solicitud_ingreso
    - fecha_elaboracion (Date)
    - foto (Binary)	
    - nombre_id (M2o)
    - cedula
    - nacionalidad
    - edad
    - sexo (Selection)
    - estadocivil (Selection)
    - problema_medico (Boolean)
    - discapacidad (Boolean)
    - nombrar_problema
    - cual_discapacidad
    - telefonos
    - correo
    - fax
    - direccion
    - nivel_estudio
    - institucion
    - titulo
    - ya_trabajo (Boolean)
    - ultimo_empleo
    - fecha_ingreso
    - fecha_egreso
    - cargo
    - telefono_trabajo
    - superior

- empleo
    - necesidad
    - contratacion_ids (M2o)
    - vacantes
    - contratacion
    - aprobado_ids (O2m)
    - espera_ids (O2m)

- contrata
    - empleo_ids (O2m)
    - contratacion 

- entrevistas
    - fech_entrevista (Datetime)
    - postulado_id (M2o)

- rechazo
    - postulado (M2o)
    - correo
    - motivo

### Cambios

#### 26/06/2017 - 30/06/2017
- Determinacion de los requisitos funcionales, no funcionales y datos persistentes
- creacion de manifest.py
- creacion de reclutamiento.py
- creacion de init.py
- Creacion de clase **postulado**
- creacion de clase **telefono**
- Creacion de clase **empleo**
- Creacion de clase **memorandum**
- Creacion de clase **Contratacion**
- Creacion de vistas formulario y lista
- creacion de repositorio
- subida de documentos al reposidorio
- Creacion de metodo *name_get()* en clase postulado
- Creacion de metodo *aprobar()* en clase postulado
- Creacion de metodo *onchange_nombre()* en clase contratacion
- Eliminacion de clase **telefono**

#### 03/07/2017 - 07/07/2017
- clase contratacion removida
- clase **solicitudIngresos** creada
- creacion de clase **fecha_entrevista**
- creacion de clase **hora_entrevista**
- Creacion de *api.constrains* para validar fechas
- Errores en las validaciones de fecha, eliminacion de *constrains*
- creacion de clase **entrevista**
- eliminacion de clases **hora_entrevista** y **fecha_entrevista**
- creacion de buttons para aprobar o rechazar postulados mediante campo booleano *aprobacion*
- Elaboracion de buttons para abrir formularios de entrevista y de solicitud de ingreso
- Creacion de clase **rechazo**
- Ordenado de postulados mediante estado de aprobado o pendiente
- Eliminada clase **memorandum**
- Error con *search_default*
- Añadidos campos a clase **solicitudIngreso**
- Eliminada vista de formulario de clase **contratacion**
- Vista calendario añadida a Entrevistas
- Calendario no especifica las horas
- Error con las identaciones

#### 10/07/2017 - 14/07/2017
- Renombrado *api.model* a *api.multi*
- Renombrado atributo *aprobacion* por *state*
- Volver a identar todo soluciona el error de identacion y el error de las *constrains*
- correccion de *search_default*
- Establecido el envio de id por contexto a los formularios nuevos
- Establecido el poder selecionar hora y fecha en el calendario
- creacion de clase **contrata** para establecer el tipo de contratacion.
- Validacion de fechas en clase **solicitudIngreso**
- Colocado boton para desactivar fechas de entrevistas no deseadas.
- Filtrado de aprobados y pendientes en la clase **empleo**
- Documentado de clases y metodos
- Añadido boton para aprobar la solicitud

#### 17/07/2017 - 21/07/2017
- Validado que al tener estudios pida nombre de la institucion y titulo obtenido
- Validado que si tiene alguna condicion medica, sea requerido el nombrarla
- Cambiado campo M2m a M2o en el tipo de contratacion de la clase **empleo**, añadido campo *related*
- Establecidos campos M2o como selection
- Añadida vista kanban en **solicitud de ingresos**
